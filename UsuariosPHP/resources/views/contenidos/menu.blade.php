<nav aria-hidden="true" class="menu" id="ui_menu" tabindex="-1">
    <div class="menu-scroll">
        <div class="menu-content">
            <a class="menu-logo" >@yield('tituloencabezado')</a>
            <ul class="nav">
                <li>
                    <a class="collapsed waves-attach" data-toggle="collapse" href="#ui_menu_admin">Usuarios</a>
                    <ul class="menu-collapse collapse" id="ui_menu_admin">
                        <li>
                            <a class="waves-attach" href="{{route('adminusuarios')}}"><span class="icon">account_box</span>Administrar Usuarios</a>
                        </li>
                        <li>
                            <a class="waves-attach" href="{{route('favoritos')}}"><span class="icon">favorite_border</span>Favoritos</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a class="collaosed waves-attach" data-toggle="collapse" href="#ui_menu_Favoritos">Favoritos</a>
                    <ul class="menu-collapse collapse" id="ui_menu_Favoritos">
                        <li>
                            <a class="waves-attach" href="{{route('favoritos')}}"><span class="icon">stars</span>Asignar Favoritos</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a class="collaosed waves-attach" data-toggle="collapse" href="#ui_menu_Pagos">Pagos</a>
                    <ul class="menu-collapse collapse" id="ui_menu_Pagos">
                        <li>
                            <a class="waves-attach" href="{{route('pagos')}}"><span class="icon">credit_card</span>Realizar Pago</a>
                        </li>
                        {{--<li>--}}
                            {{--<a class="waves-attach" href="{{route('administrarpagos')}}">Administrar Pagos</a>--}}
                        {{--</li>--}}
                    </ul>
                </li>
                {{--<li>--}}
                    {{--<a class="collapsed waves-attach" data-toggle="collapse" href="#ui_menu_reportes">Reportes</a>--}}
                    {{--<ul class="menu-collapse collapse" id="ui_menu_reportes">--}}
                        {{--<li>--}}
                            {{--<a class="waves-attach" href="javascript:void(0)">Usuarios Favoritos</a>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<a class="waves-attach" href="javascript:void(0)">Pagos de usuarios</a>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
            </ul>
        </div>
    </div>
</nav>