@extends('plantilla.principal')
@section('title')
    <title>Ingreso</title>
@endsection
@section('tituloencabezado')
    Administrar Usuarios y Pagos PHP
@endsection
@section('TituloPrincipal')
    Bienvenido al sistema para GEOPAGO
@endsection
@section('contenido')
    <div class="fbtn-container">
        <div class="fbtn-inner">
            <a class="fbtn fbtn-lg fbtn-brand-accent waves-attach waves-circle waves-light waves-effect" data-toggle="dropdown"><span class="fbtn-text fbtn-text-left">Links</span><span class="fbtn-ori icon">apps</span><span class="fbtn-sub icon">close</span></a>
            <div class="fbtn-dropup">
                <a class="fbtn waves-attach waves-circle waves-effect" href="https://bitbucket.org/marshalejrt/pruebausuariosphp" target="_blank"><span class="fbtn-text fbtn-text-left">Fork me on bitbucket</span><span class="icon">code</span></a>
                <a class="fbtn fbtn-green waves-attach waves-circle waves-effect" href="http://indexsoftwareca.com/ejdev/" target="_blank"><span class="fbtn-text fbtn-text-left">Visit My Website</span><span class="icon">link</span></a>
            </div>
        </div>
    </div>
@endsection
@section('titulopie')
    Copyright <a href="http://indexsoftwareca.com/ejdev/" target="_blank">Ejrodriguez</a> para GEOPAGO - 2017
@endsection