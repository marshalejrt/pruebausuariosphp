@extends('plantilla.principal')
@section('title')
    <title>Prueba PHP</title>
@endsection
@section('tituloencabezado')
    Pagos
@endsection
@section('TituloPrincipal')
    Administrar Pagos de Usuarios
@endsection
@section('javascript')
    <script src="js/funcionesGenerales.js"></script>
    <script>
        @if(count($errors->all())>0)
        $(function () {
                    content = "<ul>";
                    @foreach($errors->all() as $error)
                        content += "<li>{{$error}}</li>";
                    @endforeach
                    content += "</ul>";
                    $('body').snackbar({
                        alive: 4000,
                        content: content
                    });
                });
        @endif
    </script>
@endsection
@section('contenido')
    <div class="row">
        <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2">
            <section class="content-inner margin-top-no">

                    <fieldset>
                        <div class="card">
                            <div class="card-main">
                                <div class="card-inner">
                                    <form id="formPagos" action="{{route('cargarpagos')}}" method="post">
                                        <input type="hidden" value="{{csrf_token()}}" name="_token">

                                        <div class="form-group form-group-label">
                                            <label class="floating-label" for="usersel_id">Seleccione un Usuario</label>
                                            <select class="form-control" id="codigousuario" name="codigousuario" onchange="validaselect();$('#formPagos').submit();">
                                                <option></option>
                                                @foreach($usuarios as $usuario)
                                                    <option value="{{$usuario->codigousuario}}">{{$usuario->usuario}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </form>
                                </div>
                                <div class="card-inner">
                                    <p class="margin-top-no">

                                        <a type="button" style="display: none" data-toggle="modal" href="#admin_pago" onclick="
                                        $('#importe').val('');$('.operacion').val('AGREGA');" class="btn btn-green waves-attach addpago">Agregar Pagos</a>

                                    </p>
                                </div>
                            </div>
                        </div>
                    </fieldset>

                {{--<h2 class="content-sub-heading">Tables within Cards</h2>--}}
                <div class="card usuarios_registrados" name="usuarios_registrados">
                    <div class="card-main">
                        <div class="card-inner margin-bottom-no">
                            <p class="card-heading">Pagos Registrados</p>

                            <div class="card-table">
                                <div class="">
                                    <table class="table" title="Pagos Registrados">
                                        <thead>
                                        <tr>
                                            <th>Codigo Pago</th>
                                            <th>Importe</th>
                                            <th>Fecha</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <script>

                                        </script>
                                        {{$mensaje="�Est� seguro de eliminar �ste Pago?"}}
                                        @foreach($Pagos as $pago)
                                            <tr>
                                                <td id="{{$pago->codigopago}}">{{$pago->codigopago}}</td>
                                                <td id="{{$pago->codigopago}}"><strong>$ {{$pago->importe}}</strong></td>
                                                <td id="{{$pago->codigopago}}">{{$pago->fecha}}</td>
                                                <td id="{{$pago->codigopago}}">
                                                    <ul class="nav nav-list margin-no pull-right">
                                                        <li class="dropdown">
                                                            <a class="dropdown-toggle text-black waves-attach" data-toggle="dropdown"><span style="z-index: 0" class="icon">view_module</span></a>
                                                            <ul class="dropdown-menu dropdown-menu-right" style="   ">
                                                                <li>
                                                                    <a class="waves-attach modificarPago" data-toggle="modal" href="#admin_pago" data-id="{{$pago->codigopago}}" data-importe="{{$pago->importe}}" data-fecha="{{$pago->fecha}}"  ><span class="icon margin-right-sm">build</span>&nbsp;Modificar</a>
                                                                </li>
                                                                <li>
                                                                    <a class="waves-attach eliminaPago"  data-id="{{$pago->codigopago}}" onclick="$('.msg').text('Confirme eliminar el Pago'); $('#id_elimina').attr('data-id',$(this).data('id'));$('#id_elimina').attr('data-ruta','{{route('eliminarPago', ['id'=>$pago->codigopago])}}');"  href="#msgbox" data-backdrop="static" data-toggle="modal"><span class="icon margin-right-sm">delete</span>&nbsp;Eliminar</a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>


                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    <div class="card-action">
                                        <div class="card-action-btn pull-right">
                                            <a class="btn btn-flat waves-attach" href="javascript:void(0)"><span class="icon">chevron_left</span></a>
                                            <a class="btn btn-flat waves-attach" href="javascript:void(0)"><span class="icon">chevron_right</span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </section>
        </div>
    </div>
    <div aria-hidden="true" class="modal modal-va-middle fade" id="admin_pago" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-xs">
            <div class="modal-content">
                <div class="modal-heading">
                    <h2 class="modal-title">Complete los datos del Pago</h2>
                </div>
                <form action="{{route('agregarEditPago')}}" method="post">
                    <input type="hidden" value="{{csrf_token()}}" name="_token">
                    <fieldset>
                        <div class="card">
                            <div class="card-main">
                                <div class="card-inner">
                                    <div class="form-group form-group-label">
                                        <label class="floating-label" for="fecha">Fecha del Pago</label>
                                        <input class="form-control" readonly id="fecha" name="fecha" type="date" value="{{ date('Y-m-d') }}">
                                    </div>
                                    <div class="form-group form-group-label">
                                        <label class="floating-label" for="importe">Importe $</label>
                                        <input class="form-control" min="1" id="importe" name="importe" type="number" step="any" >
                                    </div>

                                    <div class="form-group form-group-label">
                                        <input type="hidden" id="operacion" name="operacion"  value="AGREGA" >
                                        <input type="hidden" id="modal_codigousuario" name="modal_codigousuario" value=""  >
                                        <input type="hidden" id="codigopago" name="codigopago" value=""  >
                                    </div>
                                </div>
                                <div class="card-inner">
                                    <p class="margin-top-no">
                                        <button type="submit"  class="btn btn-green waves- aceptar">Agregar</button>
                                        <button type="submit" style="display: none;" onclick="{{$estamodificando=true}}" class="btn btn-brand-accent waves-attach modificar">Modificar</button>
                                        <a type="button" data-dismiss="modal"  style="display: none;" onclick="$('.operacion').val('AGREGA');{{$estamodificando=false}}" class="btn btn-red waves-attach cancelar">Cancelar</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
    @if($userselect)
        <script>
           $('#codigousuario').val({{$userselect}});
           $('#modal_codigousuario').val({{$userselect}});
           $('.addpago').show();
        </script>
    @endif
    <script>
        function validaselect(){
            if($('#codigousuario').val()==0){
                $('.addfavorito').hide();
            }else{
                $('.addfavorito').show();

            }
        }

    </script>
    @if(app('request')->input('codigousuario')>0 && $userselect==0)
        <script>
            alert('{{app('request')->input('informacion')}}')
            $('#codigousuario').val({{app('request')->input('codigousuario')}});
            $('#formPagos').submit();
        </script>
    @endif
    @include('contenidos.mensaje')

@endsection

@section('titulopie')
    Copyright <a href="http://indexsoftwareca.com/ejdev/" target="_blank">Ejrodriguez</a> para GEOPAGO - 2017
@endsection
