@extends('plantilla.principal')
@section('title')
    <title>Prueba PHP</title>
@endsection
@section('tituloencabezado')
    Administración
@endsection
@section('TituloPrincipal')
    Usuarios
@endsection
@section('javascript')
    <script src="js/funcionesGenerales.js"></script>
    <script>
        @if(count($errors->all())>0)
        $(function () {
                    content = "<ul>";
                    @foreach($errors->all() as $error)
                        content += "<li>{{$error}}</li>";
                    @endforeach
                    content += "</ul>";
                    $('body').snackbar({
                        alive: 4000,
                        content: content
                    });
                });
        @endif
    </script>
@endsection
@section('contenido')
    <div class="row">
        <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2">
            <section class="content-inner margin-top-no">
                <form action="{{route('agregarEditUsuario')}}" method="post">
                    <input type="hidden" value="{{csrf_token()}}" name="_token">
                    <fieldset>
                        <div class="card">
                            <div class="card-main">
                                <div class="card-inner">
                                    <div class="form-group form-group-label">
                                        <label class="floating-label" for="usuario">Usuario</label>
                                        <input class="form-control" id="usuario" name="usuario" type="email" value="{{old('usuario')}}">
                                    </div>
                                    <div class="form-group form-group-label">
                                        <label class="floating-label" for="password">Contraseña</label>
                                        <input class="form-control" id="clave" name="clave" type="password">
                                    </div>
                                    <div class="form-group form-group-label">
                                        <label class="floating-label" for="clave_confirmation">Confirmar
                                            Contraseña</label>
                                        <input class="form-control" id="clave_confirmation"
                                               name="clave_confirmation" type="password">
                                    </div>
                                    <div class="form-group form-group-label">
                                        <label class="floating-label" for="edad">Edad</label>
                                        <input class="form-control" min="19" id="edad" name="edad" type="number" value="{{old('edad')}}">
                                        <input type="hidden" id="operacion" name="operacion"  value="AGREGA" >
                                        <input type="hidden" id="codigousuario" name="codigousuario" value=""  >
                                    </div>
                                </div>
                                <div class="card-inner">
                                    <p class="margin-top-no">
                                        <button type="submit"  class="btn btn-green waves- aceptar">Agregar</button>
                                        <button type="submit" style="display: none;" onclick="{{$estamodificando=true}}" class="btn btn-brand-accent waves-attach modificar">Modificar</button>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </form>
                <div class="row text-center">
                    <a type="button" style="display: none;" href="{{route('adminusuarios')}}" class="btn btn-brand-accent btn-red waves-attach cancelar">Cancelar</a>
                </div>
                {{--<h2 class="content-sub-heading">Tables within Cards</h2>--}}
                <div class="card usuarios_registrados" name="usuarios_registrados">
                    <div class="card-main">
                        <div class="card-inner margin-bottom-no">
                            <p class="card-heading">Usuarios Registrados</p>

                            <div class="card-table">
                                <div class="">
                                    <table class="table" title="Usuarios Registrados">
                                        <thead>
                                        <tr>
                                            <th>Codigo Usuario</th>
                                            <th>Usuario</th>
                                            <th>Edad</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <script>

                                        </script>
                                        {{$mensaje="¿Está seguro de eliminar éste Usuario?"}}
                                            @foreach($usuarios as $usuario)
                                                <tr>
                                                    <td id="{{$usuario->codigousuario}}">{{$usuario->codigousuario}}</td>
                                                    <td id="{{$usuario->codigousuario}}">{{$usuario->usuario}}</td>
                                                    <td id="{{$usuario->codigousuario}}">{{$usuario->edad}}</td>
                                                    <td id="{{$usuario->codigousuario}}">
                                                        <ul class="nav nav-list margin-no pull-right">
                                                            <li class="dropdown">
                                                                <a class="dropdown-toggle text-black waves-attach" data-toggle="dropdown"><span style="z-index: 0" class="icon">view_module</span></a>
                                                                <ul class="dropdown-menu dropdown-menu-right" style="   ">
                                                                    <li>
                                                                        <a class="waves-attach modificarUsuario" data-codigousuario="{{$usuario->codigousuario}}" data-usuario="{{$usuario->usuario}}" data-edad="{{$usuario->edad}}"  ><span class="icon margin-right-sm">build</span>&nbsp;Modificar</a>
                                                                    </li>
                                                                    <li>
                                                                        <a class="waves-attach eliminaUsuario"  data-codigousuario="{{$usuario->codigousuario}}" onclick="$('#id_elimina').attr('data-id',$(this).data('codigousuario'));$('#id_elimina').attr('data-ruta','{{route('eliminarUsuario', ['id'=>$usuario->codigousuario])}}');"  href="#msgbox" data-backdrop="static" data-toggle="modal"><span class="icon margin-right-sm">delete</span>&nbsp;Eliminar</a>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>


                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <div class="card-action">
                                        <div class="card-action-btn pull-right">
                                            <a class="btn btn-flat waves-attach" href="javascript:void(0)"><span class="icon">chevron_left</span></a>
                                            <a class="btn btn-flat waves-attach" href="javascript:void(0)"><span class="icon">chevron_right</span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </section>
        </div>
    </div>
    @include('contenidos.mensaje')
@endsection

@section('titulopie')
    Copyright <a href="http://indexsoftwareca.com/ejdev/" target="_blank">Ejrodriguez</a> para GEOPAGO - 2017
@endsection
