@extends('plantilla.principal')
@section('title')
    <title>Prueba PHP</title>
@endsection
@section('tituloencabezado')
    Favoritos
@endsection
@section('TituloPrincipal')
    Administrar Usuarios Favoritos
@endsection
@section('javascript')
    <script src="js/funcionesGenerales.js"></script>
    <script>
        @if(count($errors->all())>0)
        $(function () {
                    content = "<ul>";
                    @foreach($errors->all() as $error)
                        content += "<li>{{$error}}</li>";
                    @endforeach
                    content += "</ul>";
                    $('body').snackbar({
                        alive: 4000,
                        content: content
                    });
                });
        @endif
    </script>
@endsection
@section('contenido')
    <div class="row">
        <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2">
            <section class="content-inner margin-top-no">

                    <fieldset>
                        <div class="card">
                            <div class="card-main">
                                <div class="card-inner">
                                    <form id="formFavoritos" action="{{route('cargarfavoritos')}}" method="post">
                                        <input type="hidden" value="{{csrf_token()}}" name="_token">

                                        <div class="form-group form-group-label">
                                            <label class="floating-label" for="usersel_id">Seleccione un Usuario</label>
                                            <select class="form-control" id="codigousuario" name="codigousuario" onchange="validaselect();$('#formFavoritos').submit();">
                                                <option></option>
                                                @foreach($usuarios as $usuario)
                                                    <option value="{{$usuario->codigousuario}}">{{$usuario->usuario}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </form>
                                </div>
                                <div class="card-inner">
                                    <p class="margin-top-no">

                                        <a type="button" style="display: none" data-toggle="modal" href="#select_favorito" class="btn btn-green waves-attach addfavorito">Agregar Favoritos</a>

                                    </p>
                                </div>
                            </div>
                        </div>
                    </fieldset>

                {{--<h2 class="content-sub-heading">Tables within Cards</h2>--}}
                <div class="card usuarios_registrados" name="usuarios_registrados">
                    <div class="card-main">
                        <div class="card-inner margin-bottom-no">
                            <p class="card-heading">Usuarios Favoritos</p>

                            <div class="card-table">
                                <div class="">
                                    <table class="table" title="Usuarios Favoritos">
                                        <thead>
                                        <tr>
                                            <th>Codigo Usuario</th>
                                            <th>Usuario</th>
                                            <th>Edad</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <script>

                                        </script>
                                        {{$mensaje="�Est� seguro de eliminar �ste Favorito?"}}
                                        @foreach($Favoritos as $favorito)
                                            <tr>
                                                <td id="{{$favorito->codigousuario}}">{{$favorito->codigousuario}}</td>
                                                <td id="{{$favorito->codigousuario}}">{{$favorito->usuario}}</td>
                                                <td id="{{$favorito->codigousuario}}">{{$favorito->edad}}</td>
                                                <td id="{{$favorito->codigousuario}}">
                                                    <ul class="nav nav-list margin-no pull-right">
                                                        <li class="dropdown">
                                                            <a class="dropdown-toggle text-black waves-attach" data-toggle="dropdown"><span style="z-index: 0" class="icon">view_module</span></a>
                                                            <ul class="dropdown-menu dropdown-menu-right" style="   ">
                                                                <li>
                                                                    <a class="waves-attach eliminaUsuario"  data-id="{{$favorito->id}}" onclick="$('.msg').text('Confirme eliminar el usuario Favorito'); $('#id_elimina').attr('data-id',$(this).data('id'));$('#id_elimina').attr('data-ruta','{{route('eliminarFavorito', ['id'=>$favorito->id])}}');"  href="#msgbox" data-backdrop="static" data-toggle="modal"><span class="icon margin-right-sm">delete</span>&nbsp;Eliminar</a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>


                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    <div class="card-action">
                                        <div class="card-action-btn pull-right">
                                            <a class="btn btn-flat waves-attach" href="javascript:void(0)"><span class="icon">chevron_left</span></a>
                                            <a class="btn btn-flat waves-attach" href="javascript:void(0)"><span class="icon">chevron_right</span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </section>
        </div>
    </div>
    <div aria-hidden="true" class="modal modal-va-middle fade" id="select_favorito" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-xs">
            <div class="modal-content">
                <div class="modal-heading">
                    <h2 class="modal-title">Seleccione un Usuario para hacerlo Favorito</h2>
                </div>
                <form id="formAddFavoritos" action="{{route('agregarFavorito')}}" method="post">
                    <input type="hidden" value="{{csrf_token()}}" name="_token">
                    <input type="hidden" id="modal_codigousuario" name="modal_codigousuario" value="{{$userselect}}"  >
                    <input type="hidden" id="codigousuariofavorito" name="codigousuariofavorito" value=""  >
                    <ul class="nav">
                        @foreach($NoFavoritos as $nofavorito)
                        <li>
                            <a class="margin-bottom-sm waves-attach" data-dismiss="modal" onclick="$('#codigousuariofavorito').val({{$nofavorito->codigousuario}});$('#formAddFavoritos').submit();">
                                <div class="avatar avatar-inline margin-left-sm margin-right-sm">
                                    <img alt="alt text for username avatar" src="../images/users/avatar-001.jpg">
                                </div>
                                <span class="margin-right-sm text-black" data-codigousuario="{{$nofavorito->codigousuario}}" >{{$nofavorito->usuario}}</span>
                            </a>
                        </li>
                        @endforeach
                        <li>
                            <a class="margin-bottom-sm waves-attach" data-dismiss="modal" onclick="$(location).attr('href','{{route('adminusuarios')}}');" >
                                <div class="avatar avatar-inline margin-left-sm margin-right-sm">
                                    <span class="icon icon-lg text-black">add</span>
                                </div>
                                <span class="margin-right-sm text-black">Agregar Nuevo Usuario</span>
                            </a>
                        </li>
                    </ul>
                </form>
            </div>
        </div>
    </div>
    @if($userselect)
        <script>
           $('#codigousuario').val({{$userselect}});
           $('.addfavorito').show();
        </script>
    @endif
    <script>
        function validaselect(){
            if($('#codigousuario').val()==0){
                $('.addfavorito').hide();
            }else{
                $('.addfavorito').show();
            }
        }
    </script>
    @if(app('request')->input('codigousuario')>0 && $userselect==0)
        <script>
            alert('{{app('request')->input('informacion')}}')
            $('#codigousuario').val({{app('request')->input('codigousuario')}});
            $('#formFavoritos').submit();
        </script>
    @endif
    @include('contenidos.mensaje')

@endsection

@section('titulopie')
    Copyright <a href="http://indexsoftwareca.com/ejdev/" target="_blank">Ejrodriguez</a> para GEOPAGO - 2017
@endsection