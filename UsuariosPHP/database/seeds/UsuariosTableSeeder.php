<?php

use Illuminate\Database\Seeder;

class UsuariosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('usuarios')->insert([
            'usuario' => 'admin@prueba.com',
            'clave' => bcrypt('123456'),
            'edad' => 25,
        ]);
    }
}
