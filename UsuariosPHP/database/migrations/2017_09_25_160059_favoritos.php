<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Favoritos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('favoritos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('codigousuario')->unsigned();
            $table->foreign('codigousuario')->references('codigousuario')->on('usuarios');
            $table->integer('codigousuariofavorito')->unsigned();
            $table->foreign('codigousuariofavorito')->references('codigousuario')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('favoritos');
    }
}
