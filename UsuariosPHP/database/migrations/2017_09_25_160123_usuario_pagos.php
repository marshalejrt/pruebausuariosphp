<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UsuarioPagos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuariospagos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('codigopago')->unsigned();
            $table->foreign('codigopago')->references('codigopago')->on('pagos');
            $table->integer('codigousuario')->unsigned();
            $table->foreign('codigousuario')->references('codigousuario')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('usuariospagos');
    }
}
