$(document).ready(function () {


});

$(document).on("click", ".modificarUsuario", function () {
    id=$(this).data("codigousuario");
    $("#usuario").val($(this).data('usuario'));
    $("#edad").val($(this).data('edad'));
    //---------------
    modificar(id);
});
$(document).on("click", ".modificarPago", function () {
    id=$(this).data("id");
    $("#fecha").val($(this).data('fecha'));
    $("#importe").val($(this).data('importe'));
    //---------------
    $("#operacion").val('MODIFICA');
    $("#codigopago").val(id);

    $('.aceptar').css({
        'display': 'none'
    });
    $('.modificar').show();
    $('.cancelar').show();
});



$(document).on("click", ".modificarAsignatura", function () {
    id=$(this).data("id");
    $("#nombre").val($(this).data('nombre'));
    $("#valor").val($(this).data('valor'));
    //---------------
    modificar(id);
});

function obtenerFavoritos(id){
    $.ajax({
        url: './favoritos',
        headers: {'X-CSRF-TOKEN': $('input[name=_token]').val()},
        data: {
            id: id
        },
        type: 'POST',
        datatype: 'JSON',
        success: function (resp) {
            console.log(resp);
        }
    });
}
function modificar(id){
    $("#operacion").val('MODIFICA');
    $("#codigousuario").val(id);
    $('.usuarios_registrados').css({
        'display': 'none'
    });
    $('.aceptar').css({
        'display': 'none'
    });
    $('.modificar').show();
    $('.cancelar').show();
}


$(document).on("click", ".inscribirMaterias", function () {
    id=$(this).data("id");

    //---------------
    $('#materias').modal('show');
});

