<?php

namespace App;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;


class Usuarios extends Model implements Authenticatable
{
    //
    use \Illuminate\Auth\Authenticatable;
    protected $primaryKey = 'codigousuario';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'usuario','clave','edad'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'clave', 'remember_token',
    ];
    public function getAuthPassword() {
        return $this->clave;
    }
}