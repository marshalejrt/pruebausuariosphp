<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Favoritos extends Model
{
    //
    public function usuarios()
    {
        return $this->belongsTo('App\Usuarios', 'codigousuariofavorito', 'codigousuario');


    }
}
