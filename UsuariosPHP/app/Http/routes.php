<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('login');
});


//-----------------RUTAS USUARIOS-----------------------------------------
Route::get('/adminusuarios', function(){
    $usuarios=\App\Usuarios::all();
    $mensaje='';
    $accion='';
    $estilo='';
    $estamodificando=false;
    return view('adminusuarios')->with(['usuarios'=>$usuarios,'mensaje'=>$mensaje,'accion'=>$accion,'estilo'=>$estilo,'estamodificando'=>$estamodificando]);
})->name('adminusuarios')->middleware('auth');

Route::post('/agregarEditUsuario', [
    'uses' => 'ControladorUsuario@postAgregarEditUsuario',
    'as' => 'agregarEditUsuario',
    'middleware' => 'auth'
]);

Route::get('/eliminarUsuario/{id}', [
    'uses' => 'ControladorUsuario@getEliminarUsuario',
    'as' => 'eliminarUsuario',
    'middleware' => 'auth'
]);



//-----------------RUTAS FAVORITOS-----------------------------------------
Route::post('/favoritos', [
    'uses' => 'ControladorFavoritos@postFavoritos',
    'as' => 'cargarfavoritos',
    'middleware' => 'auth'
]);

Route::get('/favoritos', function(){
    $usuarios=\App\Usuarios::all();
    $mensaje='�Est� seguro de eliminar �ste Favorito?';
    $accion='';
    $estilo='';
    $userselect=0;
    $Favoritos=array();
    $NoFavoritos=array();
    return view('adminfavoritos')->with(['usuarios'=>$usuarios,'Favoritos'=>$Favoritos,'NoFavoritos'=>$NoFavoritos,'mensaje'=>$mensaje,'accion'=>$accion,'estilo'=>$estilo,'userselect'=>$userselect]);
})->name('favoritos')->middleware('auth');

Route::post('/agregarFavorito', [
    'uses' => 'ControladorFavoritos@postAgregarFavorito',
    'as' => 'agregarFavorito',
    'middleware' => 'auth'
]);

Route::get('/traerfavoritos/{id}', [
    'uses' => 'ControladorFavoritos@getFavoritos',
    'as' => 'traerfavoritos',
    'middleware' => 'auth'
]);

Route::get('/eliminarFavorito/{id}', [
    'uses' => 'ControladorFavoritos@getEliminarFavorito',
    'as' => 'eliminarFavorito',
    'middleware' => 'auth'
]);




//-----------------RUTAS PAGOS-----------------------------------------
Route::post('/pagos', [
    'uses' => 'ControladorPagos@postPagos',
    'as' => 'cargarpagos',
    'middleware' => 'auth'
]);

Route::get('/pagos', function(){
    $usuarios=\App\Usuarios::all();
    $mensaje='�Est� seguro de eliminar �ste Pago?';
    $accion='';
    $estilo='';
    $userselect=0;
    $Pagos=array();
    $NoFavoritos=array();
    return view('adminpagos')->with(['usuarios'=>$usuarios,'Pagos'=>$Pagos,'NoFavoritos'=>$NoFavoritos,'mensaje'=>$mensaje,'accion'=>$accion,'estilo'=>$estilo,'userselect'=>$userselect]);
})->name('pagos')->middleware('auth');

Route::post('/agregarEditPago', [
    'uses' => 'ControladorPagos@postAgregarPago',
    'as' => 'agregarEditPago',
    'middleware' => 'auth'
]);

Route::get('/traerpago/{id}', [
    'uses' => 'ControladorPagos@getPagos',
    'as' => 'traerpagos',
    'middleware' => 'auth'
]);

Route::get('/eliminarPago/{id}', [
    'uses' => 'ControladorPagos@getEliminarPago',
    'as' => 'eliminarPago',
    'middleware' => 'auth'
]);



//-----------------RUTAS INICIO DE SESION-----------------------------------------
Route::get('//inicio', function () {
    return view('index');
})->name('index')->middleware('auth');


Route::post('/ingresar', [
    'uses' => 'ControladorSesion@postIngresar',
    'as' => 'ingresar'
]);
Route::get('/logout', function () {
    \Illuminate\Support\Facades\Auth::logout();
    return redirect()->route('login');
})->name('logout');
Route::get('/login', function () {
    return view('login');
})->name('login');