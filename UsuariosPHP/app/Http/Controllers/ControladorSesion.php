<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use DB;
class ControladorSesion extends Controller
{
    /**
     * postIngresar
     * Crea una sesi�n de usuario con token
     * @param  request
     * @return informacion | view->index
     */
    public function postIngresar(Request $request)
    {
        //Validamos los datos de entrada
        $this->validate($request, [
            'usuario'=>'required|email',
            'clave' => 'required'
        ]);
        //si son correcto iniciamos sesi�n
        if (Auth::attempt(['usuario' => $request["usuario"], 'password' => $request["clave"]])) {
            return redirect()->route('index');
        }
        return redirect()->back()->with(['informacion' => 'Usuario o Clave incorrecto, por favor verifique...', 'tipo' => 'error']);
        //return redirect()->route('index');
    }

}
