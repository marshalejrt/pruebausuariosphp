<?php

namespace App\Http\Controllers;

use App\Favoritos;
use App\Usuarios;
use Illuminate\Http\Request;

use App\Http\Requests;
use DB;

class ControladorUsuario extends Controller
{
    /**
     * postAgregarEditUsuario
     * Administra Usuarios agrega y modifica un usuario
     * @param  request
     * @return informacion
     */
    public function postAgregarEditUsuario(Request $request)
    {
        //Preparamos el parametro Operacion
        $op=$request["operacion"];
        //Validamos si se va a agregar un registro
        if ($op=="AGREGA"){

            //Validamos los datos de entrada
            $this->validate($request, [
                'usuario' => 'required|unique:usuarios|email',
                'clave' => 'required|min:5|max:10|confirmed',
                'edad' => 'required',
            ]);
            //Validamos que no exista el mismo email asignado a otro usuario
            if(Usuarios::where('usuario',$request["usuario"])->first()){
                return redirect()->back()->with(['informacion' => 'El usuario '. $request["usuario"] .' ya existe en el sistema, verifique', 'tipo' => 'error']);
            }
            //Creamos el usuario
            $usuario = new Usuarios();
            $usuario->usuario = $request["usuario"];
            $usuario->clave = bcrypt($request["clave"]);
            $usuario->edad = $request["edad"];
            $usuario->save();
            return redirect()->back()->with(['informacion' =>'Registro agregado con Satisfactoriamente...']);
        }else{//MODIFICA UN REGISTRO

            //Validamos que no exista el mismo email asignado a otro usuario
            if(Usuarios::where('usuario',$request['usuario'])->where('codigousuario','!=',$request["codigousuario"])->first()){
                return redirect()->back()->with(['informacion' => 'El usuario '. $request["usuario"] .' ya existe en el sistema, intente con otro', 'tipo' => 'error']);
            }

            //validamos los datos de entrada
            $this->validate($request, [
                'usuario' => 'required|email',
                'clave' => 'required|min:5|max:10|confirmed',
                'edad' => 'required',
            ]);

            //Modificamos el registro
            if ($usuario = Usuarios::find($request["codigousuario"])) {
                $usuario->usuario = $request["usuario"];
                $usuario->clave = bcrypt($request["clave"]);
                $usuario->edad = $request["edad"];
                $usuario->update();
                return redirect()->back()->with(['informacion' => 'El Registro se ha Modificado satisfactoriamente...']);
            }
            return redirect()->back()->with(['informacion' =>'Ocurrio un Error al Modificar el Registro, Verifique...', 'tipo' => 'error']);
        }

    }

    /**
     * getEliminarUsuario
     * Elimina un usuario de la base de datos
     * @param  $id
     * @return informacion
     */
    public function getEliminarUsuario($id)
    {
        //Validamos para no eliminar el usuario administrador
        if ($id==1) {
            return redirect()->back()->with(['informacion' => 'No se puede eliminar el usuario Administrador, Intente con otro usuario', 'tipo' => 'error']);
        }
        //Validamos que el usuario no sea favorito de otro usuario
        $favoritos=Favoritos::where('codigousuariofavorito',$id)->first();
        if($favoritos){
            return redirect()->back()->with(['informacion' => 'No se puede eliminar un Usuario Favorito de Otro', 'tipo' => 'error']);
        }
        //Buscamos el usuario a eliminar
        if (!$usuario = Usuarios::find($id)) {
            return redirect()->back()->with(['informacion' => 'Ocurrio un Error al intentar Eliminar el Registro, Verifque...', 'tipo' => 'error']);
        }
        $usuario->delete();
        return redirect()->back()->with(['informacion' => 'El Registro se ha eliminado satisfactoriamente...', 'tipo' => 'ok']);
    }


}
