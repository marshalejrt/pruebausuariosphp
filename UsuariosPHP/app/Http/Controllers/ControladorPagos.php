<?php

namespace App\Http\Controllers;

use App\Favoritos;
use App\Pagos;
use App\Usuarios;
use App\Usuariospagos;
use Illuminate\Http\Request;

use App\Http\Requests;
use DB;

class ControladorPagos extends Controller
{

    /**
     * postPagos
     * Devuelve la informaci�n necesaria para administrar
     * los pagos a los usuarios
     * @param  request
     * @return informacion
     */
    public function postPagos(Request $request){
        //obtenemos los pagos de un usuario espec�fico
        $Pagos=$this->ObtenerPagos($request["codigousuario"]);
        $usuarios=\App\Usuarios::all();
        $mensaje='';
        $accion='';
        $estilo='';


        return View('adminpagos')->with(['usuarios'=>$usuarios,'Pagos'=>$Pagos,'mensaje'=>$mensaje,'accion'=>$accion,'estilo'=>$estilo,'userselect'=>$request["codigousuario"]]);

    }

    /**
     * ObtenerPagos
     * Devuelve los pagos de un usuario
     *
     * @param  codigousuario
     * @return Pagos
     */
    public function ObtenerPagos($codigousuario){
        //Buscamos la lista de pagos para un usuario
        //relacionado con la tabla pagos
        $usersPagos=Usuariospagos::with('pagos')->where('codigousuario',$codigousuario)->get();
        $Pagos=array();
        //armamos el array y guardamos el usuario para futuras modificaciones
        foreach($usersPagos as $Pago){
            $Pago->pagos['codigousuario']=$Pago->codigousuario;
            array_push($Pagos,$Pago->pagos);
        }
        return $Pagos;
    }

    /**
     * postAgregarPago
     * Administra los pagos
     *Agrega y modifica pagos a un usuario
     * @param  $request
     * @return Informacion
     */
    public function postAgregarPago(Request $request)
    {


        $op=$request["operacion"];
        if ($op=="AGREGA"){

            //validamos los datos de entrada
            $this->validate($request, [
                'fecha' => 'required|date|after:yesterday',
                'importe' => 'required|min:1'
            ]);

            //Creamos el nuevo pago
            $pago = new Pagos();
            $pago->fecha = $request["fecha"];
            $pago->importe = $request["importe"];
            $pago->save();

            //Asociamos el pago creado con el usuario
            $usuariopago=new Usuariospagos();
            $usuariopago->codigopago=$pago->codigopago;
            $usuariopago->codigousuario=$request["modal_codigousuario"];
            $usuariopago->save(['timestamps' => false]);

            return redirect()->route('cargarpagos', ['codigousuario' => $request["modal_codigousuario"],'informacion' => 'Registro Agregado satisfactoriamente...']);

        }else{//MODIFICA UN REGISTRO
            //Validamos los datos
            $this->validate($request, [
                'fecha' => 'required|after:yesterday',
                'importe' => 'required|min:1'
            ]);
            //Buscamos el Pago y Actualizamos el registro
            if ($pago = Pagos::find($request["codigopago"])) {
                $pago->fecha= $request["fecha"];
                $pago->importe= $request["importe"];
                $pago->update();
                return redirect()->route('cargarpagos', ['codigousuario' => $request["modal_codigousuario"],'informacion' => 'Registro Actualizado satisfactoriamente...']);
            }
            return redirect()->route('cargarpagos', ['codigousuario' => $request["modal_codigousuario"],'informacion' => 'No se encuentra el registro a Actualizar!']);
        }


    }


    /**
     * getEliminarPago
     * Elimina un pago asociado a un usuario
     *
     * @param  $id
     * @return Informacion
     */
    public function getEliminarPago($id)
    {
        //Buscamos el pago
        if (!$Pago = Pagos::find($id)) {
            return redirect()->back()->with(['informacion' => 'No se encuentra el pago solicitado...', 'tipo' => 'error']);
        }
        //Validamos que est� asociado con el usuario actual
        if (!$UsuarioPago = Usuariospagos::where('codigopago',$id)->first()) {
            return redirect()->back()->with(['informacion' => 'No se encuentra informaci�n de pago asociado, Verifque...', 'tipo' => 'error']);
        }
        //Obtenemos el usuario actual para devolverlo a la vista
        $codigousuario=$UsuarioPago->codigousuario;
        $UsuarioPago->delete();
        $Pago->delete();

        return redirect()->route('cargarpagos', ['codigousuario' => $codigousuario,'informacion' => 'Registro Eliminado satisfactoriamente...']);

    }


}
