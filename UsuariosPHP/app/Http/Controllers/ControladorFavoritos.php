<?php

namespace App\Http\Controllers;

use App\Favoritos;
use App\Usuarios;
use Illuminate\Http\Request;

use App\Http\Requests;
use DB;

class ControladorFavoritos extends Controller
{

    /**
     * postFavoritos
     * Busca información necesaria para administrar Favoritos
     *
     * @param  $request
     * @return Informacion
     */
    public function postFavoritos(Request $request){
        $Favoritos=$this->ObtenerFavoritos($request["codigousuario"]);
        $NoFavoritos=$this->ObtenerNoFavoritos($request["codigousuario"]);
        $usuarios=\App\Usuarios::all();
        $mensaje='';
        $accion='';
        $estilo='';
        error_log(json_encode($NoFavoritos));

        return View('adminfavoritos')->with(['usuarios'=>$usuarios,'Favoritos'=>$Favoritos,'NoFavoritos'=>$NoFavoritos,'mensaje'=>$mensaje,'accion'=>$accion,'estilo'=>$estilo,'userselect'=>$request["codigousuario"]]);

    }

    /**
     * ObtenerFavoritos
     * Devuelve los Favoritos de un usuario
     *
     * @param  codigousuario
     * @return Favoritos
     */
    public function ObtenerFavoritos($codigousuario){
        //Buscamos la lista de favoritos para un usuario
        //relacionado con la tabla usuarios
        $usersFavoritos=Favoritos::with('usuarios')->where('codigousuario',$codigousuario)->get();
        $Favoritos=array();
        //armamos el array y guardamos el id favorito para futuras modificaciones
        foreach($usersFavoritos as $Favorito){
            $Favorito->usuarios['id']=$Favorito->id;
            array_push($Favoritos,$Favorito->usuarios);
        }
        return $Favoritos;
    }

    /**
     * ObtenerNOFavoritos
     * Devuelve los usuarios que no son Favoritos
     *
     * @param  codigousuario
     * @return NoFavoritos
     */
    public function ObtenerNOFavoritos($codigousuario){
        //Buscamos la lista de favoritos para un usuario
        $Favoritos=$this->ObtenerFavoritos($codigousuario);
        $codfavoritos=array();
        array_push($codfavoritos,$codigousuario);
            foreach($Favoritos as $Favorito){
                array_push($codfavoritos,$Favorito->codigousuario);
            }
        //Buscamos los usuarios que no son favoritos
        $NoFavoritos=Usuarios::whereNotIn('codigousuario',$codfavoritos)->get();

        return $NoFavoritos;
    }

    /**
     * postAgregarFavorito
     * Agrega un usuario favorito
     *
     * @param  codigousuario
     * @return NoFavoritos
     */
    public function postAgregarFavorito(Request $request)
    {
        $NuevoFavorito=new Favoritos();
        $NuevoFavorito->codigousuario=$request["modal_codigousuario"];
        $NuevoFavorito->codigousuariofavorito=$request["codigousuariofavorito"];
        $NuevoFavorito->save(['timestamps' => false]);

        return redirect()->route('cargarfavoritos', ['codigousuario' => $request["modal_codigousuario"],'informacion' => 'Registro Agregado satisfactoriamente...']);

    }


    /**
     * getEliminarFavorito
     * Elimina un usuario favorito
     *
     * @param  codigousuario
     * @return NoFavoritos
     */
    public function getEliminarFavorito($id)
    {
        //Buscamos el Favorito a eliminar
        if (!$favorito = Favoritos::find($id)) {
            return redirect()->back()->with(['informacion' => 'Ocurrio un Error al intentar Eliminar el Registro, Verifque...', 'tipo' => 'error']);
        }
        //Eliminamos
        $codigousuario=$favorito->codigousuario;
        $favorito->delete();

        return redirect()->route('cargarfavoritos', ['codigousuario' => $codigousuario,'informacion' => 'Registro Eliminado satisfactoriamente...']);
    }


}
